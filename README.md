# gitlab cypress-example-included [![pipeline status](https://gitlab.com/cypress-io/cypress-example-included/badges/master/pipeline.svg)](https://gitlab.com/cypress-io/cypress-example-included/commits/master)

This GitLab repository is showing how to run Cypress end-to-end tests on GitLab CI without installing anything by using [cypress/included](https://github.com/cypress-io/cypress-docker-images/tree/master/included) Docker image.

See [.gitlab-ci.yml](.gitlab-ci.yml) file.
